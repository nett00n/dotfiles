-- Set up basic settings
vim.cmd('syntax on')           -- Enable syntax highlighting
-- vim.cmd('filetype plugin indent on') -- Enable filetype detection, plugins, and indentation
vim.opt.tabstop = 2           -- Set tab stop to 2 spaces
vim.opt.softtabstop = 2       -- Set soft tab stop to 2 spaces
vim.opt.shiftwidth = 2        -- Set indentation width to 2 spaces
vim.opt.expandtab = true      -- Use spaces instead of tabs
vim.opt.autoindent = true     -- Enable auto-indentation
vim.opt.number = true         -- Display line numbers

-- Search
vim.opt.hlsearch = true       -- Highlight search results
vim.opt.incsearch = true      -- Show partial matches for search queries as you type

-- Mouse
vim.opt.mouse = 'a'           -- Enable mouse support in Neovim


