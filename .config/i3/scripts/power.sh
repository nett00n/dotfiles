#! /bin/sh

LOGOUT_TEXT="󰈆 Log Out"
SUSPEND_TEXT="󰒲 Suspend"
RESTART_TEXT="󰜉 Restart"
POWEROFF_TEXT="⏻ Power OFF"

LOGOUT_COMMAND="i3-msg exit"
SUSPEND_COMMAND="i3lock -i ~/Pictures/backgrounds/VIM.png && systemctl suspend"
RESTART_COMMAND="sudo reboot"
POWEROFF_COMMAND="systemctl poweroff"

chosen=$(printf "${LOGOUT_TEXT}\n${SUSPEND_TEXT}\n${RESTART_TEXT}\n${POWEROFF_TEXT}" | rofi -dmenu -i -theme-str '@import "~/.config/rofi/powermenu.rasi"')

case "$chosen" in
  "${LOGOUT_TEXT}") $( echo ${LOGOUT_COMMAND} ) ;;
  "${SUSPEND_TEXT}") $( echo ${SUSPEND_COMMAND} ) ;;
  "${RESTART_TEXT}") $( echo ${RESTART_COMMAND} ) ;;
  "${POWEROFF_TEXT}") $( echo ${POWEROFF_COMMAND} ) ;;
	*) exit 1 ;;
esac
