# This file is typically used to set up configurations and preferences specific to the Nu shell itself. It can contain settings related to the behavior of Nu, such as prompt customization, aliases, functions, and plugin configurations. Essentially, config.nu is where you can define settings that affect how Nu operates for your user account.
$env.config = {
  show_banner: false
  table: {
    mode: light
  }
  datetime_format: {
    table: '%Y-%m-%d %H:%M:%S %z'
    normal: '%Y-%m-%d %H:%M:%S %z %a'
  }
}
$env.PATH = [
  /bin
  /data/data/com.termux/files/usr/bin
  /opt/homebrew/bin
  /sbin/
  /usr/bin
  /usr/local/bin
  ~/.local/bin/
  ~/Library/Python/3.10/bin
  ~/Library/Python/3.11/bin
  ~/Library/Python/3.9/bin
]

starship init nu | save -f ~/.cache/starship/init.nu
