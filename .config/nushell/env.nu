# This file, on the other hand, is more focused on defining environment variables that affect the behavior of commands and programs run within the Nu shell environment. It allows you to set environment variables that are accessible to Nu and any commands or scripts executed within Nu. This file is particularly useful for setting up environment variables required by specific applications or for customizing the environment for your workflow.

use std log

# def show-log [] {
#     log debug "Debug message"
#     log info "Info message"
#     log warning "Warning message"
#     log error "Error message"
#     log critical "Critical message"
# }

alias apk = sudo apk
alias apt = sudo nala
alias chmod = sudo chmod
alias chown = sudo chown
alias compose = sudo docker-compose
alias dive = sudo docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest
alias docker = sudo docker

mkdir ${HOME}/.cache/starship
source ${HOME}/.cache/starship/init.nu

def internet-check [] {
  # Checks for internet-connection
  let INTERNET_STATUS = (^ping -c 1 -t 10 1.1.1.1 | complete).exit_code
  if $INTERNET_STATUS != 0 { log warning "Internet is OFF"; return 0 } else {log info "Internet is ON"; return 1}
}

def git-repo-update [REPO_PATH] {
  cd $REPO_PATH
  let REPO_IS_MODIFIED = (git diff-index --quiet HEAD | complete).exit_code
  if $REPO_IS_MODIFIED != 0 { log warning "There are uncommited changes in repo" ; return }
  let INTERNET_STATUS = (^ping -c 1 -t 10 1.1.1.1 | complete).exit_code
  if $INTERNET_STATUS != 0 { log warning "Internet is OFF"; return 1 }
  log info "All changes in repo are committed. Sync it with origin"
  let PULL_RESULT = (git pull | complete)
  let PUSH_RESULT = (git push | complete)
  cd -
}

NU_LOG_LEVEL=WARNING internet-check
NU_LOG_LEVEL=WARNING git-repo-update "${HOME}/personal-env/dotfiles"
