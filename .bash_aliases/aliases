#!/usr/bin/env bash

# Aliases for navigation
alias ......="cd../../../../.."
alias ......="cd../../../../../.."
alias .....="cd../../../.."
alias ....="cd../../.."
alias ...="cd../.."

alias date-standard='date --rfc-3339=seconds'
alias date-short="date +'%Y%m%d%H%M%S'"
alias todo-txt='$EDITOR /home/nett00n/.local/share/todo.txt/todo.txt'

# Aliases for package managers
alias apk="sudo apk"
alias apt="sudo nala"
alias dnf="sudo dnf"
alias nixos-rebuild="sudo nixos-rebuild"

# Aliases for rights
alias chgrp='chgrp --preserve-root'
alias chmod='sudo chmod --preserve-root'
alias chown='sudo chown --preserve-root'

# Aliases for file operations
alias cp='cp -i'
alias ln='ln -i'
alias mv='mv -i'
alias rm='rm -I --preserve-root'

# Aliases for show mounts
alias mounts='grep "^/dev/" /proc/mounts | column -t'

# Terminal things
reset-terminal-settings() {
  # List of essential variables to keep
  local essential_vars=("BASH" "BASHOPTS" "BASH_VERSION" "COLUMNS" "DIRSTACK"
                        "EUID" "GROUPS" "HISTCONTROL" "HISTFILE" "HISTFILESIZE"
                        "HISTSIZE" "HOME" "HOSTNAME" "IFS" "LINES" "LOGNAME"
                        "MACHTYPE" "MAIL" "MAILCHECK" "OPTERR" "OPTIND" "OSTYPE"
                        "PATH" "PIPESTATUS" "PPID" "PS1" "PS2" "PS4" "PWD"
                        "SHELL" "SHELLOPTS" "SHLVL" "TERM" "UID" "USER")

  # Unset all variables except essential ones
  for var in $(compgen -v); do
    if [[ ! " ${essential_vars[*]} " =~ " ${var} " ]]; then
      unset "$var"
    fi
  done

  # Reset the terminal
  reset

  # Reload .bashrc or .bash_profile, depending on what exists
  if [ -f "$HOME/.bashrc" ]; then
    source "$HOME/.bashrc"
  elif [ -f "$HOME/.bash_profile" ]; then
    source "$HOME/.bash_profile"
  else
    printf "Warning: No .bashrc or .bash_profile found in the home directory.\n"
  fi

  printf "Session reset and configuration reloaded.\n"
}

alias r=reset-terminal-settings

# Aliases for system information
alias nocomment="grep -Ev '''^(#|$)'''"

# Aliases for clipboard
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# Git aliases
alias ga="git -c color.status=always add"
alias gc="git -c color.status=always commit"
alias gcd='git add . && pre-commit && git commit -m "$(date --rfc-3339=seconds)" && git push'
alias gd="git -c color.status=always diff"
alias gs="git -c color.status=always status"
alias gu="git -c color.status=always pull && git push"
alias guc="git -c color.status=always reset HEAD~"

# Terraform aliases
alias tfp="terraform-plan"
alias tfa="terraform-apply"
alias tfd="terraform-destroy"

# Docker aliases
alias act="gh act"
alias compose="sudo docker compose"
alias dive="sudo docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest"
alias docker="sudo docker"
alias entropy='docker run --rm -v $(pwd):/data ewenquim/entropy /data'
alias hadolint='docker run --rm -i hadolint/hadolint < Dockerfile'

# AWS profile management
aws-profile-select() {
  # Display the currently selected profile if it's set and valid
  if [ -n "$AWS_PROFILE" ] && grep -q "profile $AWS_PROFILE" "${HOME}/.aws/config"; then
    printf "☁️  Currently selected AWS Profile: \033[1m%s\033[0m\n" "$AWS_PROFILE"
  else
    printf "☁️  AWS Profile is not selected or invalid.\n"
  fi

  # List available profiles
  aws-profile-list

  # Prompt user to select a valid profile
  read -r -p "☁️  Enter AWS Profile to select (or press Enter to keep current): " selected_profile

  # If the user presses Enter without input, keep the current profile
  if [ -z "$selected_profile" ] && [ -n "$AWS_PROFILE" ]; then
    printf "☁️  Keeping current AWS Profile: \033[1m%s\033[0m\n" "$AWS_PROFILE"
    return
  fi

  # Check if the entered profile is valid
  if grep -q "profile $selected_profile" "${HOME}/.aws/config"; then
    export AWS_PROFILE="$selected_profile"
    printf "☁️  AWS Profile set to: \033[1m%s\033[0m\n\n" "$AWS_PROFILE"
  else
    printf "☁️  Invalid profile. No changes made.\n"
  fi
}

aws-profile-list() {
  # List available AWS profiles, or provide feedback if none are found
  CONFIG_FILE="${HOME}/.aws/config"
  if [ -f "$CONFIG_FILE" ]; then
    printf "☁️  List of AWS profiles:\n"
    grep -E '^\[profile ' "$CONFIG_FILE" | sed 's/.*profile \(.*\)\]/- \1/'
  else
    printf "☁️  No AWS profiles found. Ensure that %s exists.\n" "$CONFIG_FILE"
  fi
}

# Terraform plan and apply
function terraform-wrapper() {
  # Check if a valid action is provided
  local action="$1"
  shift  # Shift to access additional arguments
  case "$action" in
    plan|plan-destroy|apply|destroy) ;;
    *)
      echo "Usage: terraform-wrapper plan|plan-destroy|apply|destroy [-var=\"foo=bar\"]"
      return 1
      ;;
  esac

  # Determine Terraform action-specific options
  local terraform_cmd="terraform"
  local tfplan_file="tfplan"
  local extra_args=()

  if [ "$action" = "plan-destroy" ]; then
    terraform_cmd="terraform plan -destroy"
    action="plan"  # Rename to plan for further actions
  elif [ "$action" = "plan" ]; then
    terraform_cmd="terraform plan"
    extra_args+=("-out=$tfplan_file")
  elif [ "$action" = "apply" ]; then
    terraform_cmd="terraform apply"
    extra_args+=("$tfplan_file")  # Apply the tfplan file if created
  elif [ "$action" = "destroy" ]; then
    terraform_cmd="terraform destroy"
  fi

  # Step 1: Identify *.tfvars files other than terraform.tfvars
  local tfvars_files
  tfvars_files=($(find . -maxdepth 1 -name '*.tfvars' ! -name 'terraform.tfvars'))

  # Step 2: Prompt user if multiple tfvars files are found
  local selected_tfvars_args=()
  if [ "${#tfvars_files[@]}" -gt 1 ]; then
    printf "Multiple .tfvars files found:\n"
    for file in "${tfvars_files[@]}"; do
      printf "  - %s\n" "$file"
    done

    # Prompt for selection of tfvars files
    printf "Enter the names of the .tfvars files to use, separated by spaces or commas (press Enter to use all): "
    read -r user_selection

    if [ -z "$user_selection" ]; then
      # If user input is empty, use all found .tfvars files
      for file in "${tfvars_files[@]}"; do
        selected_tfvars_args+=("-var-file=$file")
      done
    else
      # Split user input on spaces or commas
      IFS=', ' read -r -a selected_files <<< "$user_selection"
      for file in "${selected_files[@]}"; do
        file=$(echo "$file" | xargs)  # Trim whitespace
        if [[ -f "$file" ]]; then
          selected_tfvars_args+=("-var-file=$file")
        else
          printf "Warning: File '%s' not found and will be skipped.\n" "$file"
        fi
      done
    fi
  elif [ "${#tfvars_files[@]}" -eq 1 ]; then
    # If only one additional tfvars file is found, select it automatically
    selected_tfvars_args+=("-var-file=${tfvars_files[0]}")
  fi


  # Step 3: Collect remaining arguments as additional options for Terraform
  local other_args=("$@")

  # Step 4: Run the Terraform and related commands
  tflint --minimum-failure-severity=error || { echo "tflint failed"; return 1; }
  terraform fmt || { echo "terraform fmt failed"; return 1; }
  aws-profile-select || { echo "aws-profile-select failed"; return 1; }
  terraform init || { echo "terraform init failed"; return 1; }
  terraform validate || { echo "terraform validate failed"; return 1; }

  # Step 5: Execute the appropriate Terraform command with all arguments
  $terraform_cmd "${selected_tfvars_args[@]}" "${other_args[@]}" "${extra_args[@]}"
}

# Git repository update
git-repo-update() {
  # Validate repository path
  local REPO_PATH="$1"
  if [ -z "$REPO_PATH" ]; then
    printf "Error: No repository path provided.\nUsage: git-repo-update <path-to-repo>\n"
    return 1
  elif [ ! -d "$REPO_PATH/.git" ]; then
    printf "Error: '%s' is not a valid git repository.\n" "$REPO_PATH"
    return 1
  fi

  cd "$REPO_PATH" || return 1  # Change directory to REPO_PATH or return if failed

  # Check for uncommitted changes
  if ! git diff-index --quiet HEAD; then
    printf "Warning: There are uncommitted changes in the repository. Please commit or stash changes before updating.\n"
    cd - >/dev/null || return 1  # Return to previous directory
    return 1
  fi

  # Check internet connectivity
  if ! ping -c 1 -W 10 1.1.1.1 &>/dev/null; then
    printf "Warning: No internet connection. Cannot sync with origin.\n"
    cd - >/dev/null || return 1
    return 1
  fi

  # Display current branch
  local CURRENT_BRANCH
  CURRENT_BRANCH=$(git symbolic-ref --short HEAD)
  printf "Info: Syncing branch '%s' with origin.\n" "$CURRENT_BRANCH"

  # Pull the latest changes
  git pull || {
    printf "Error: Failed to pull changes from origin.\n"
    cd - >/dev/null || return 1
    return 1
  }

  # Check if there are new commits to push
  if git log origin/"$CURRENT_BRANCH"..HEAD --oneline | grep -q .; then
    # Only push if SSH key exists
    if [ -f "${HOME}/.ssh/id_ed25519" ]; then
      git push || printf "Error: Failed to push changes to origin.\n"
    else
      printf "Warning: No SSH key found. Push is not permitted.\n"
    fi
  else
    printf "Info: No new commits to push.\n"
  fi

  # Return to the previous directory
  cd - >/dev/null || return 1
  return 0
}


internet-check() {
  # Checks for internet connection
  if ping -c 1 -W 10 1.1.1.1 &> /dev/null; then
    printf "\e[32mInfo: Internet is \e[1mON\e[0m\n"  # Green color for "ON"
    return 0  # Success
  else
    printf "\e[33mWarning: Internet is \e[1mOFF\e[0m\n"  # Yellow color for "OFF"
    return 1  # Failure
  fi
}

# Backup a file
backup-files() {
  cp $@ $@.backup-"$(date +%y%m%d)"
}

stacks () {
  # Check if a docker command (e.g., up, down) is provided
  if [ -z "$1" ]; then
    printf "Usage: stacks <docker-command> (e.g., up, down, restart)\n"
    return 1
  fi

  # Check if docker-compose or compose is installed
  if ! command -v docker-compose &>/dev/null && ! command -v compose &>/dev/null; then
    printf "Error: docker-compose or compose command not found.\n"
    return 1
  fi

  # Store the current directory
  CurrentDir="${PWD}"

  # Find all docker-compose files
  ComposeFiles=$(find /Stacks/ -maxdepth 2 -type f \( -name docker-compose.yml -o -name compose.yml \))

  # Check if any compose files were found
  if [ -z "$ComposeFiles" ]; then
    printf "No docker-compose.yml or compose.yml files found in /Stacks/\n"
    return 1
  fi

  # Loop through each docker-compose file found
  for ComposeFile in $ComposeFiles; do
    ComposeDir=$(dirname "$ComposeFile")
    # Output directory in bold
    printf "\n🐳 Processing stack in directory: \033[1m%s\033[0m\n" "$ComposeDir"

    # Navigate to the compose directory
    cd "$ComposeDir" || {
      printf "Warning: Could not change to directory \033[1m%s\033[0m. Skipping.\n" "$ComposeDir"
      continue
    }

    # Run the docker-compose command with provided arguments
    docker-compose "$@" 2>/dev/null || compose "$@" 2>/dev/null

    # Return to the original directory
    cd "$CurrentDir" || return 1
  done
}

auto-select-editor() {
  PossibleEditorsList="code nvim nano vim vi"

  for editor in $PossibleEditorsList; do
      if command -v "$editor" >/dev/null 2>&1; then
          export EDITOR="$editor"
          break
      fi
  done

  echo "The default editor is set to: $EDITOR"
}

auto-select-terminal() {

  terminals="kitty alacritty gnome-terminal konsole xfce4-terminal xterm uxterm"

  for terminal in $terminals; do
      if command -v "$terminal" >/dev/null 2>&1; then
          export TERMINAL="$terminal"
          break
      fi
  done

  echo "The default terminal emulator is set to: $TERMINAL"

}

function search-lib-in-nixos () {
  test ! -z "$1" || ( echo "Input lib to search" && return 0 )
  LIB_NAME="$1"
  nix-locate "${LIB_NAME}" | awk '{print$1}' | sort | uniq
}

function nix-rebuild() {
  # Determine the NixOS configuration path
  if [ -d "${HOME}/personal-env/nixos.desktop/" ]; then
    NixOsConfig="/home/nixos-config"
  elif [ -d "/home/nixos-config" ]; then
    NixOsConfig="${HOME}/personal-env/nixos.desktop/"
  else
    NixOsConfig="github:nett00n/nixos.desktop"
  fi

  # Determine the hostname, prompt for it if running on "nixos"
  if [ "$(hostname)" = "nixos" ]; then
    printf "Enter host name: "
    read -r HostName
  else
    HostName="$(hostname)"
  fi

  # Set default command if none is provided
  Command="${1:-test}"

  # Execute the nixos-rebuild command
  sudo nixos-rebuild "$Command" --flake "${NixOsConfig}#${HostName}"
}

pomodoro() {
  nohup shellmodoro "$@" > /dev/null 2>&1 &
  disown
  echo "Shellmodoro is running in the background."
}
