#!/usr/bin/env bash

POLYBAR_PATH=$(which polybar)

killall -q polybar

echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log
polybar -q i3_bar 2>&1 | tee -a /tmp/polybar1.log & disown

