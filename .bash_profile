export HISTCONTROL=ignoredups
export HISTFILESIZE=
export PATH=${HOME}/.local/bin:${HOME}/Library/Python/3.9/bin:${PATH}
export TZ=Asia/Tbilisi
export XDG_DATA_DIRS=$XDG_DATA_DIRS:/usr/share:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share
if [ -t 0 ]; then
    for i in $(ls ${HOME}/.bash_aliases/); do source ${HOME}/.bash_aliases/$i; done
    internet-check
    git-repo-update "${HOME}/personal-env/dotfiles/"
    auto-select-editor
    auto-select-terminal
fi

uname -a | grep -i Darwin \
>/dev/null ||
(
    if [ $(pgrep -u "$USER" -x "ssh-agent" | wc -l) -gt 1 ]; then
        pgrep -u "$USER" -x "ssh-agent" | xargs kill
        find /tmp -type d -name "ssh-*" -user "$USER" -exec rm -rf {} \;
    fi
    if pgrep -u "$USER" -x "ssh-agent" > /dev/null; then
      eval $(cat "$HOME/.ssh/environment")
    else
        find /tmp -type d -name "ssh-*" -user "$USER" -exec rm -rf {} \;
        ssh-agent -s > "$HOME/.ssh/environment"
      eval $(cat "$HOME/.ssh/environment")
    fi
    if ! ssh-add -l 2>/dev/null | grep -q "The agent has no identities"; then
        ssh-add
    fi
)
