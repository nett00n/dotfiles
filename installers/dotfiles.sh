#!/bin/sh

directory=${HOME}

# Iterate over all symbolic links in the specified directory
find "$directory" -type l | while read -r symlink; do
    # Check if the symbolic link is broken
    if [ ! -e "$symlink" ]; then
        echo "Broken symlink: $symlink"
        rm "$symlink"
    fi
done


mkdir -p "${HOME}/personal-env"
cd "${HOME}/personal-env" || exit 1
test -d dotfiles || git clone git@gitlab.com:nett00n/dotfiles.git
cd "${HOME}/personal-env/dotfiles" || exit 1
for i in $(find . -type d | grep -v '^./.git/' | grep -v '^./installers' | sed 's@^./@@g'); do mkdir -p "${HOME}"/"${i}"; done
for i in $(find . -type f | grep -v 'README.md' | grep -v '^./.git/' | grep -v 'install' | sed 's@^./@@g'); do echo "Creating symlink ${i}"; ln -s -f "${PWD}"/"${i}" "${HOME}"/"${i}"; done
dasel -f .config/ksnip/ksnip.yaml -w toml > "${HOME}/.config/ksnip/ksnip.conf"
