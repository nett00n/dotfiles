#!/usr/bin/env bash
which brew || (echo "Brew not found. Installing it" && /bin/sh -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)")

ln -s -F "${HOME}/personal-env/dotfiles/.config/nushell/env.nu" "/Users/nett00n/Library/Application Support/nushell/env.nu"
ln -s -F "${HOME}/personal-env/dotfiles/.config/nushell/config.nu" "/Users/nett00n/Library/Application Support/nushell/config.nu"

brew install \
    alt-tab \
    firefox \
    font-fira-code-nerd-font \
    gh \
    glab \
    iterm2 \
    neovim \
    starship \
  && echo
starship init nu > "${HOME}/.cache/starship/init.nu"
