#!/bin/sh
# Preconfigure termux environment to use default config

mkdir -p "${HOME}/personal-env/"
cd "${HOME}/personal-env/" || exit 1
test -d termux-nerd-installer || git clone http://github.com/notflawffles/termux-nerd-installer.git
cd "termux-nerd-installer" || exit 1
make install
termux-nerd-installer install fira-code
pkg install \
  curl \
  dasel \
  dog \
  gh \
  git \
  glab-cli \
  hugo \
  mc \
  neovim \
  net-tools \
  nushell \
  openssh \
  python-pip \
  starship \
  tmux \
  wget \
  -y

pip install pre-commit
cd ${HOME}/personal-env/
test -d ${HOME}/storage || termux-setup-storage
echo >/data/data/com.termux/files/usr/etc/motd
