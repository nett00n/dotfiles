#!/bin/sh
cat /etc/os-release | grep 'NAME="Ubuntu"' -i > /dev/null || exit 0
sudo pip install --user apt-smart; sudo /root/.local/bin/apt-smart -a
mkdir -p ~/personal-env/
cd ~/personal-env/
sudo apt update -qq
sudo apt install \
  net-tools \
  neovim \
  git \
  curl \
  tmux \
  wget \
  curl \
  mc \
  gh \
  hugo \
  python3-pip \
  jq \
  -y -qq
 
pip install --user \
  pre-commit \
  lastversion \

cd ~/personal-env/
